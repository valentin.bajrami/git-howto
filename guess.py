#!/usr/bin/env python3

import random



comp_number = random.randrange(0, 11)
first_attempt = ""
second_attempt = ""
last_attempt = ""
restart = False
trys = 0


print("Guess the number between 1 and 10")

while restart != True:
   
    first_attempt = int(input("First try: "))

    if first_attempt == comp_number:
        print("You win first try!")
        try_again = input("Do you want another go? [Y/N]: ")
    else:
        print("Too bad.. Try again")
        second_attempt = int(input("Second try: "))
    if second_attempt == comp_number:
        print("You win second try!")
        try_again = input("Do you want another go? [Y/N]: ")
    else:
        print("This is your last try!!!")
        last_attempt = int(input("Last try: "))
    if last_attempt == comp_number:
        print("That was close but you won!")
        try_again = input("Do you want another go? [Y/N]: ")
    elif last_attempt != comp_number and first_attempt != comp_number and second_attempt != comp_number:
            print("computer won :|")
            try_again = input("Do you want another go? [Y/N]: ")
    if try_again.lower == "N":
        restart = False
    else:
        trys += 1 
        print (f"try number: {trys}")
