#!/usr/bin/env bash

mapfile -t pyfiles < <(git diff-tree --no-commit-id -r HEAD --name-only '*.py')

if (( ${#pyfiles[@]} > 1)); then
    printf "Running linter on changed files... %s\n" "${pyfiles[@]}"
    pylint-3 --evaluation="100.0 - ((float(5 * error + warning + refactor + convention) / statement) * 100)" ${pyfiles[@]} \
    | awk -F'/' '/^Your code has/{match($0, /([0-9]{1,3})/, arr)
        if (arr[0] >= 80) 
          {
            print "Code quality acceptable. Nice job, bro.";
            exit 0
          }
        else 
          { 
            print "Code quality not good enough... Try harder!"
            exit 1
          }
        }'
else
    printf '%s\n' 'No changes found'
    exit 0
fi
