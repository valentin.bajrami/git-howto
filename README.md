### A compact howto interact with git

For this purpose, I've created the following project. You can start here: [git-howto](https://gitlab.com/valentin.bajrami/git-howto.git)
git@gitlab.com:valentin.bajrami/git-howto.git

First, lets clone the project

```
git clone git@git@gitlab.com:valentin.bajrami/git-howto.git
```

This will create a directory called `git-howto` within your current directory

After cloning the repo, let's change directory to it:

```
cd git-howto
```

See what branch we are on (this will return main)

```
git branch
```

Lets now branch from the main branch, this means we will create a new branch from main

```
git checkout -b feature/my-new-awesome-feature
```

The above will result in

```
Switched to a new branch 'feature/my-new-awesome-feature'
```

Now running `git branch` again will show the new branch. When creating a new branch you are automatically switched to that branch. The result is:

```
* feature/my-new-awesome-feature
  main
```

Let's now create a file in the new branch

```
touch new_script
```

Add the following lines to the script and save it.

```
#!/usr/bin/env bash

printf '%s\n' "Hello World"
```

Now we can add the script to the new branch using:

```
git add new_script
```

Running `git status` yields:

```
On branch feature/my-new-awesome-feature
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   new_script
```

The `new_script` is ready to be committed. This can be done as follows:

```
git commit -m "Print me hello world script"
```

The result is something like:

```
[feature/my-new-awesome-feature aee1db2] Print me hello world script
 1 file changed, 4 insertions(+)
 create mode 100644 new_script
```

Now we can run `git log` to see the commit ID, author, date etc..

```
commit aee1db2cc66b496fee85d0412849fa67fbed3099 (HEAD -> feature/my-new-awesome-feature)
Author: Valentin Bajrami <foo.bar@domaint.tld>
Date:   Wed Jan 12 17:12:37 2022 +0100

    Print me hello world script
```

I have a fancy one-liner for git if you are interested in:

```
alias graph='git log --pretty=oneline --graph --decorate --all'
```

This will show the output as:

```
* aee1db2cc66b496fee85d0412849fa67fbed3099 (HEAD -> feature/my-new-awesome-feature) Print me hello world script
* d1b2e4551899f4b28a042f703840ae33dacc21eb (origin/main, main) Initial setup, add README.md
```

### Squash commits
Imagine you are working on this awesome feature and you were adding a line or two to the script and committed the changes. Now you noticed you made a typo or wanted to add another extra line to this script. Later on, you added another line and committed the changes. These commits will pile up and and you'll able to see them using `git log`. Here is where `squash` comes in picture. You can squash all these commits all together. Let's demonstrate this in a second.

1. Let's create a new branch and work from there (before doing this make sure you switched to the main branch and did a `git pull`)

```
git checkout -b feature/improve-my-awesome-feature
```

2. Let's update the script by adding a few lines. The end result looks something like:

```
$> git log
commit 8c8a267cf095f2f270629904df32ba4fa002f508 (HEAD -> feature/improve-my-awesome-feature, origin/feature/improve-my-awesome-feature)
Author: Valentin Bajrami <foo.bar@domaint.tld>
Date:   Mon Jan 17 22:10:25 2022 +0100

    Add another new line  and count to 10 (update 2)

commit 08d0b910e75b4ba2d008d37195913226ca3a073a
Author: Valentin Bajrami <foo.bar@domaint.tld>
Date:   Mon Jan 17 22:08:43 2022 +0100

    Add another new line (update 1)

commit 20dd382aaf657dd48e4281333b3fcc89bdd6f9fa
Author: Valentin Bajrami <foo.bar@domaint.tld>
Date:   Mon Jan 17 22:04:15 2022 +0100

    Add another line to the script

commit 1448e76920f815b99a6083c60dc1ac2e50447fdc
Merge: d1b2e45 18be7af
Author: Valentin Bajrami <foo.bar@domaint.tld>
Date:   Wed Jan 12 16:24:10 2022 +0000

    Merge branch 'feature/my-new-awesome-feature' into 'main'

    Feature/my new awesome feature

    See merge request valentin.bajrami/git-howto!1
```

3. Now we need to squash the commits to a single one. So basically this is our latest commit:

```
commit 8c8a267cf095f2f270629904df32ba4fa002f508 (HEAD -> feature/improve-my-awesome-feature, origin/feature/improve-my-awesome-feature)
Author: Valentin Bajrami <foo.bar@domaint.tld>
Date:   Mon Jan 17 22:10:25 2022 +0100

    Add another new line  and count to 10 (update 2)
```

And we want to squash them all together. We can use `git rebase -i [hash of commit we want to squash]`.

4. Use `rebase -i [commit-hash]`.

```
git rebase -i 1448e76920f815b99a6083c60dc1ac2e50447fdc
```

.. and the following screen appears:

```
pick 20dd382 Add another line to the script
pick 08d0b91 Add another new line (update 1)
pick 8c8a267 Add another new line  and count to 10 (update 2)

# Rebase 1448e76..8c8a267 onto 1448e76 (3 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup [-C | -c] <commit> = like "squash" but keep only the previous
#                    commit's log message, unless -C is used, in which case
#                    keep only this commit's message; -c is same as -C but
#                    opens the editor
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified); use -c <commit> to reword the commit message
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
```

To squash the commits we can modify the following 3 lines (leave the rest as is):

```
pick 20dd382 Add another line to the script
pick 08d0b91 Add another new line (update 1)
pick 8c8a267 Add another new line  and count to 10 (update 2)
...
...
```

to

```
pick 20dd382 Add another line to the script
s 08d0b91 Add another new line (update 1)
s 8c8a267 Add another new line  and count to 10 (update 2)
```

After saving and quitting, the following appears:

```
$> git rebase -i 1448e76920f815b99a6083c60dc1ac2e50447fdc
[detached HEAD c3dac16] Add another line to the script
 Date: Mon Jan 17 22:04:15 2022 +0100
 1 file changed, 5 insertions(+)
Successfully rebased and updated refs/heads/feature/improve-my-awesome-feature.
```

5. Use `git push` to push the changes. If `git push` doesn't work you can run `git push --force`  to force the squash.

6. Running `git log` again shows the following

```
$> git log
commit c3dac164c77a04aae3976d36a0a1c8d30b3e37fd (HEAD -> feature/improve-my-awesome-feature, origin/feature/improve-my-awesome-feature)
Author: Valentin Bajrami <foo.bar@domaint.tld>
Date:   Mon Jan 17 22:04:15 2022 +0100

    Add another line to the script

    Add another new line (update 1)

    Add another new line  and count to 10 (update 2)
```

### How to pull from main branch

If you are working on a branch but you want to sync with the main branch and still keep your changes, then you need to run the following

```
git pull origin main
```

Here we are telling git (while we're in our branch)  that our origin (source) is main and pull it from there into the branch.

### Git stash and what is it

> Use git stash when you want to record the current state of the working directory and the index, but want to go back to a clean working directory. The command saves your local modifications away and reverts the working directory to match the HEAD commit.

#### Demonstration

So I was working on this `new_script` project within `feature/improve-my-awesome-feature` branch but I need to work on `foo_bar`. I don't want to commit changes on the `new_script` yet so I am going to stash the work so I can return back to it later on.

Using `git status`  we will see the following

```
On branch feature/improve-my-awesome-feature
Your branch is up to date with 'origin/feature/improve-my-awesome-feature'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	modified:   new_script

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	foo_bar
```

Now we want to stash `new_script` and we do this as follows:

```
git stash push -m "stash work on new_script for later"
```

> **_NOTE_:**: If `git stash push -m "..."` doesn't work then you are using an old git. You still can use `git stash save` but it is deprecated as of 2.15.x/2.16

Now we can list the stashed worker using `git stash list`. The result is:

```
stash@{0}: On improve-my-awesome-feature: stash work on new_script for later
```

Running `git status` again we no longer see the `new_script` file listed.

```
git status
On branch feature/improve-my-awesome-feature
Your branch is up to date with 'origin/feature/improve-my-awesome-feature'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	foo_bar
```

Since we need to work on `foo_bar` file we now create a new branch and work on that.

```
git checkout -b feature/brand-new-branch
```

`git status` will now shows

```
On branch feature/brand-new-branch
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	foo_bar

nothing added to commit but untracked files present (use "git add" to track)
```

After finishing work on `foo_bar` we now can add the file to be committed.

```
git add foo_bar
```

... and commit it using `git commit -m "Add foo_bar file"`:

```
[feature/brand-new-branch 5930a4f] Add foo_bar file
 1 file changed, 2 insertions(+)
 create mode 100644 foo_bar
```

... and push it using  `git push`

```
/\COOL-COMPANY - Unauthorized access prohibited
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 338 bytes | 338.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
remote:
remote: View merge request for feature/brand-new-branch:
remote:   https://gitlab.org/valentin.bajrami/git-howto/-/merge_requests/5
remote:
To git@gitlab.com:valentin.bajrami/git-howto.git
   5b36567..5930a4f  feature/brand-new-branch -> feature/brand-new-branch
```

Finally we can go back to our `new_script`  and work on that. To do so, we will switch to our previes branch and run:
1. `git branch feature/improve-my-awesome-feature`
2. `git stash pop stash{0}`

> **_NOTE:_**:  `git stash pop stash{0}` will bring back our file and empty the stack. To apply a stash and keep it in the stash stack you can run `git stash apply stash{0}`

For now we'll just run `git stash pop stash{0}`

```
git stash pop stash@{0}
On branch feature/improve-my-awesome-feature
Your branch is up to date with 'origin/feature/improve-my-awesome-feature'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   new_script
```

As show here,  `new_script` is now visible and we can continue work. Also `git stash list` is now ampty since we used `pop` instead of `apply`
