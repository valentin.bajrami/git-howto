#!/bin/bash

existing_files=""
for file in $(git diff-tree --no-commit-id --name-only -r HEAD | grep .py$); do test -f ${file} && existing_files="${existing_files} ${file}"; done
if [ -z "${existing_files}" ]
then
    echo "No changes found..."
    exit 0
else
    echo "Running linter on changed files... ${existing_files}"
    pylint-3 --evaluation="100.0 - ((float(5 * error + warning + refactor + convention) / statement) * 100)" ${existing_files} > /tmp/pylint-output
    cat /tmp/pylint-output
    result=$(awk -F'/' '/^Your code has/{match($0, /([0-9]{1,3})/ , arr); print arr[0]}' /tmp/pylint-output)
    rm /tmp/pylint-output
    if (( result >= 80 ))
    then
        echo "Code quality acceptable. Nice job, bro."
        exit 0
    else
        echo "Code quality not good enough... Try harder!"
        exit 1
    fi
fi
